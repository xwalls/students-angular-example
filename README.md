# Students Angular Example

## How to run this example

We need to clone two repositories and do checkout to the **main** branch:

- [students-angular-example](https://gitlab.com/xwalls/students-angular-example)
- [students-web-api-example](https://gitlab.com/xwalls/students-web-api-example)

## Requirements:
  - [Docker 20.10.*](https://docs.docker.com/engine/install/)
  - [node 14.18.* and npm](https://nodejs.org/es/)
  - [angular CLI](https://angular.io/cli)
  - Port 4200 and 8181 available
  - To have cloned the frontend and backend.

## 1. Run the backend
Execute this on the command line. This will build the docker image. 
``` bash
cd students-web-api-example
make build
```
This will execute the ASP.NET Web API as a docker container running on the port 8181.
```
make run
```
If needed we have an extra command to stop the container.
```
make stop
```

## 2. Run the frontend
``` bash
cd students-angular-example
npm start
```

## 4. Open your browser
We need to be sure to have the port 4200 available.Then.
Go to <http://localhost:4200>
