export const environment = {
  production: true,
  API_BASE_URL: 'http://localhost:8181/',
  WEATHER_API_KEY: '456346c9d774f47628dea8dff50c30c9',
  WEATHER_BASE_API: 'https://api.openweathermap.org/data/2.5/'
};
