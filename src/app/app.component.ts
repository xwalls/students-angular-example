import { Component, OnInit } from '@angular/core';
import { FileUploadService } from './file-upload.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'students-angular-example';
  students: any = null;
  best_student: any = null;
  worst_student: any = null;
  general_score = 0;
  
  constructor(private _fileUploadService: FileUploadService) { }

  ngOnInit(): void {
    this._fileUploadService.studentData$.subscribe((data: any) => {
      this.students = data.students;
      this.best_student = data.best_student;
      this.worst_student = data.worst_student;
      this.general_score = data.general_score;
    });
  }

}
