import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams  } from '@angular/common/http';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }
  
  getCurrentWeather(lat: Number, lon: Number) {
    return this.http
      .get(`${environment.WEATHER_BASE_API}weather?lat=${lat}&lon=${lon}&appid=${environment.WEATHER_API_KEY}&units=metric`);
  }

}
