import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.scss']
})
export class WeatherWidgetComponent implements OnInit {
  public weatherData: any = null;

  constructor(private _weatherService: WeatherService ) { 
  }

  ngOnInit(): void {
    this.getCurrentPosition()
  }
  geolocationSupport() {
    return 'geolocation' in navigator
  }

  getCurrentPosition() {
    const defaultOptions = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 1000000,
    }
    if (!this.geolocationSupport()) throw new Error('No hay soporte de geolocalización en tu navegador')
    
    return new Promise<any>((resolve, reject) => {
      navigator.geolocation.getCurrentPosition((position) => {
        const lat = position.coords.latitude
        const lon = position.coords.longitude
        this._weatherService.getCurrentWeather(lat, lon).subscribe((data: any) => {
          this.weatherData = data;
        });
        resolve(position)
      }, () => {
        reject('no hemos podido obtener tu ubicación')
      }, defaultOptions)
    })
  }

}
