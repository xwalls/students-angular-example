import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams  } from '@angular/common/http';
import { Observable, throwError as observableThrowError, Subject } from 'rxjs';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  private _studentsData = new Subject<string>();
  public studentData$ = this._studentsData.asObservable();
  private studentsUrl = 'api/Students';

  constructor(private http: HttpClient) { }

  public post(formData: FormData) {
    let params = new HttpParams();

    const options = {
      params: params,
      reportProgress: true
    };

    return this.http
      .post<any>(environment.API_BASE_URL + this.studentsUrl+ "/upload-file", formData, options)
      .subscribe((data:any) => this._studentsData.next(data));
  }

}
