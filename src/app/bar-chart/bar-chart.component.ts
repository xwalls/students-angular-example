import { Component, OnChanges, Input  } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnChanges {

  @Input()
  students: any;

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
    
  public barChartLabels: Label[] = ['Excelent [10-9]', 'Good [8-6]', 'In Danger [5-0]'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
    
  public barChartData: ChartDataSets[] = [];
    
  constructor() {}
    
  ngOnChanges(): void {
    let scores1 = 0;
    let scores2 = 0;
    let scores3 = 0;

    this.students.forEach((student: any) => {
      if(student.score >= 9 && student.score <= 10) scores1++;
      if(student.score >= 6 && student.score <= 8) scores2++;
      if(student.score >= 0 && student.score <= 5) scores3++;
    });
    this.barChartData = [
      { data: [scores1, 0, 0], label: 'Excelent scores' },
      { data: [0, scores2, 0], label: 'Good scores' },
      { data: [0, 0, scores3], label: 'In Danger scores' },
    ]
  }
}
